# Installation

## last release version
```
curl -s https://gitlab.com/private-it/code-generator/raw/master/generator.php > generator
chmod +x generator
sudo mv ./generator /usr/local/bin/
```

## dev-version
```
curl -s https://gitlab.com/private-it/code-generator/raw/develop/generator.php > generator
chmod +x generator
sudo mv ./generator /usr/local/bin/
```

# Usage

Run command in work directory

```
generator
generator short-name
generator generator.json short-name
````

## Test
```
cd example
generator
```