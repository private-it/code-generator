#!/usr/bin/env php
<?php
/**
 * RUN:
 * - ./generator
 * - ./generator short-name
 * - ./generator generator.json short-name
 */

$baseDir = getcwd();
$confGeneratorFile = $baseDir . '/generator.json';
if (sizeof($argv) == 3) {
    if (substr($argv[2], 0, 1) != '/') {
        $confGeneratorFile = $baseDir . '/' . $argv[1];
    } else {
        $confGeneratorFile = $argv[1];
    }
}
var_dump($confGeneratorFile);
if (!is_file($confGeneratorFile)) {
    die('Configuration file "generator.json" not found!');
}

$confGenerator = json_decode(file_get_contents($confGeneratorFile), true);
if (empty($confGenerator) || !is_array($confGenerator)) {
    die('Configuration not valid!');
}
if (sizeof($argv) == 3) {
    $conf = $confGenerator[$argv[2]];
} elseif (sizeof($argv) == 2) {
    $conf = $confGenerator[$argv[1]];
} else {
    $conf = reset($confGenerator);
}

if (is_array($conf['source'])) {
    $urlRefresh = $conf['source']['urlRefresh'] . '?' . http_build_query($conf['source']['params']);
    $urlGenerate = $conf['source']['urlGenerate'] . '?' . http_build_query($conf['source']['params']);
} else {
    $urlRefresh = $conf['refresh'];
    $urlGenerate = $conf['source'];
}

$outDir = $baseDir . DIRECTORY_SEPARATOR . $conf['outPath'] . DIRECTORY_SEPARATOR;

echo 'For refresh data: ' . $urlRefresh . PHP_EOL;

$context = null;
if (file_exists($outDir . 'LucidChart.xml')) {
    $postData = file_get_contents($outDir . 'LucidChart.xml');
    $opts = array('http' =>
        array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query([
                'data' => $postData
            ])
        )
    );
    $context = stream_context_create($opts);
}

$zipFileContent = file_get_contents($urlGenerate, false, $context);
if (strlen($zipFileContent) < 512) {
    die('Zip content length < 1024' . PHP_EOL . PHP_EOL . $zipFileContent);
}

$zipFilePath = tempnam(sys_get_temp_dir(), 'GII');

file_put_contents($zipFilePath, $zipFileContent);
if (strlen(file_get_contents($zipFilePath)) < 512) {
    die('Zip file size < 512');
}

$zip = new ZipArchive();

if ($zip->open($zipFilePath) === TRUE) {
    echo PHP_EOL;
    echo 'Start ' . date('Y-m-d H:i:s') . PHP_EOL;
    echo PHP_EOL;
    echo 'Update files:' . PHP_EOL;
    for ($i = 0; $i < $zip->numFiles; $i++) {
        $fileName = $zip->getNameIndex($i);
        $fileInfo = pathinfo($fileName);

        $filePath = $outDir . $fileName;
        $fileDir = dirname($filePath);

        if (!is_dir($fileDir)) {
            mkdir($fileDir, 0700, true);
        }

        copy("zip://" . $zipFilePath . "#" . $fileName, $filePath);
        echo $filePath . PHP_EOL;
    }
    $zip->close();
    echo PHP_EOL;
    echo 'End: ' . date('Y-m-d H:i:s') . PHP_EOL;
    echo 'Success';
} else {
    echo 'Error';
}
echo PHP_EOL;